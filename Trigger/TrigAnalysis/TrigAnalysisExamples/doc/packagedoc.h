/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TrigAnalysisExamples_page TrigAnalysisExamples Package
@page TrigAnalysisExamples_page TrigDecisionTool Package

@section TrigAnalysisExamples_TrigAnalysisExampleIntro Introduction
This package contains examples of usage of TrigDecisionTool.


Find more user documentation at the page:

https://twiki.cern.ch/twiki/bin/view/Atlas/TrigDecisionTool15




*/
